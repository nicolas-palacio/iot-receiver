package iot_receiver_core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import iot_receiver_core.observer.Observable;

public class Device extends Thread implements Observable<Receiver>{
	private int id;
	private List<Receiver> observers;
	private Acquisition actualMeasure;
	
	
	public Device(Integer id) {
		this.id=id;
		this.observers=new ArrayList<>();
		this.actualMeasure=new Acquisition(id,0.0);
	}
	
	
	public Acquisition generateReading() {
		Random r= new Random();
		double randomValue = 10.0 + (40.0 - 10.0) * r.nextDouble();
		Acquisition measure=new Acquisition(this.id,randomValue);
		
		notifyObservers(measure);	
		
		return measure;
	}


	public Integer getID() {
		return this.id;
	}
	
	public Acquisition getMeasure() {
		return this.actualMeasure;
	}


	@Override
	public void addObserver(Receiver observer) {
		this.observers.add(observer);
		
	}


	@Override
	public void removeObserver(Receiver observer) {
		this.observers.remove(observer);
		
	}
	
	private void notifyObservers(Acquisition reading) {
		for(Receiver receiver:this.observers) {
			receiver.receive(reading);
		}
		
	}
	
	@Override
	public void run() {
		try {
			this.actualMeasure=this.generateReading();
			System.out.println("Measure "+this.actualMeasure.toString());
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
