package iot_receiver_core;

public class Acquisition {
	private Integer deviceID;
	private Double temperature;
	
	public Acquisition(Integer deviceID,Double temperature) {
		this.deviceID=deviceID;
		this.temperature=temperature;
	}

	public Integer getDeviceID() {
		return deviceID;
	}

	public Double getTemperature() {
		return temperature;
	}

	@Override
	public String toString() {
		return "Measure [deviceID=" + deviceID + ", temperature=" + temperature + "]";
	}
		

}
