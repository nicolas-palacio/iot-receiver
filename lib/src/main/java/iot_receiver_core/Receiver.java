package iot_receiver_core;

import java.util.ArrayList;
import java.util.List;

import iot_receiver_core.discovery.ObserverPlugin;
import iot_receiver_core.observer.EventListener;
import iot_receiver_core.observer.Observable;

public class Receiver implements Observable<ObserverPlugin>,EventListener{
	private Acquisition lastMeasure;
	private List<ObserverPlugin>observers;
		
	public Receiver() {
		this.lastMeasure=new Acquisition(0,0.0);
		this.observers=new ArrayList();
	}
	
	@Override
	public void addObserver(ObserverPlugin t) {
		this.observers.add(t);
		
	}
	
	@Override
	public void removeObserver(ObserverPlugin t) {
		// TODO Auto-generated method stub
		
	}
	

	public List<ObserverPlugin> getObservers() {
		return observers;
	}

	private void notifyObservers() {
		for(ObserverPlugin observer:this.observers) {
			observer.update(lastMeasure);
		}
	}

	@Override
	public void receive(Acquisition measure) {
		if(measure==null) {
			throw new NullPointerException("Measure can't be null");
		}
		
		if(measure.getDeviceID()==null) {
			throw new NullPointerException("Device ID can't be null");
		}
		
		if(measure.getTemperature()==null) {
			throw new NullPointerException("Temperature can't be null");
		}
		
		if(measure.getTemperature().TYPE!=double.class) {
			throw new NullPointerException("Temperature must be a Double");
		}
		
		this.lastMeasure=measure;

		
		if(!this.observers.isEmpty()) {		
			notifyObservers();
		}
				
	}

	public Acquisition getLastMeasure() {
		return this.lastMeasure;
	}

	
}
