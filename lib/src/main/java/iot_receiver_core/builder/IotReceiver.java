package iot_receiver_core.builder;

import java.util.List;
import java.util.Set;

import iot_receiver_core.Device;
import iot_receiver_core.Receiver;
import iot_receiver_core.discovery.ObserverPlugin;

public class IotReceiver {
	private List<Device> thermometer;
	private Receiver receiver;
		
	public IotReceiver(List<Device> thermometer,Receiver receiver) {
		this.thermometer = thermometer;
		this.receiver=receiver;
	}
	
	public List<Device> getThermometer() {
		return thermometer;
	}

	
	public Receiver getReceiver() {
		return receiver;
	}
	
	public void startEvents() {
		while(true) {			
			this.thermometer.get(0).run();
			//this.thermometer.get(1).run();
		}
	}


	public static class IotReceiverBuilder implements IBuilder<IotReceiver>{
		private List<Device> thermometers;
		private Receiver receiver;
		
		public IotReceiverBuilder() {
			
		}
		
		public IotReceiverBuilder setThermometers(List<Device>thermometers) {
			this.thermometers=thermometers;
			return this;
		}
		
		public IotReceiverBuilder setReceiver(Receiver receiver) {
			this.receiver=receiver;
			return this;
		}
		
		/*public IotReceiverBuilder setNotifiers(Set<Notifier>notifiers) {
			this.notifiers=notifiers;
			return this;
		}*/
				
		
		@Override
		public IotReceiver build() {
			return new IotReceiver(thermometers,receiver);
		}


	}

}
