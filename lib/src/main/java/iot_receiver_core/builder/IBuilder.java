package iot_receiver_core.builder;

public interface IBuilder<T> {
	
	public T build();
	
}
