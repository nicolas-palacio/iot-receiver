package iot_receiver_core;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import iot_receiver_core.builder.IotReceiver;
import iot_receiver_core.discovery.Discovery;
import iot_receiver_core.discovery.ObserverPlugin;


public class Configuration {
	//"./src/main/resources"
	public static IotReceiver initialize(String path) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Discovery finder=new Discovery();
		Set<ObserverPlugin> set=finder.find(path);		
		
		
		Receiver receiver=new Receiver();
		for(ObserverPlugin obs:set) {
			receiver.addObserver(obs);
		}
		
		List<Device> thermometers=new ArrayList<>();
		thermometers.add(new Device(001));
		//thermometers.add(new Device(002));
		
		thermometers.get(0).addObserver(receiver);
		//thermometers.get(1).addObserver(receiver);
		
		IotReceiver iotReceiver= new IotReceiver.IotReceiverBuilder()
				.setThermometers(thermometers)
				.setReceiver(receiver)
				.build();
						
		
		return iotReceiver;
	}



}
