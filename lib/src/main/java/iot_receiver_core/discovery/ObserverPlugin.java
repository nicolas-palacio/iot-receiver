package iot_receiver_core.discovery;
import javax.swing.JPanel;

import iot_receiver_core.Acquisition;

public interface ObserverPlugin {
	
	void update(Acquisition acquisition);
	JPanel getJPanel();
	boolean isEnable();
	void changeState(boolean state);
}

