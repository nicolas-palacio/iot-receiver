package iot_receiver_core.discovery;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import iot_receiver_core.builder.IotReceiver;


public class Discovery {
	
	public Set<ObserverPlugin> find(String path) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
		Set<ObserverPlugin> result= new HashSet<>();
		Class<?> cls=null;
		File file=new File(path);
		
		if(file.isFile()) {
			throw new IllegalArgumentException("The argument must be a directory path");
		}
		
		if(!file.exists()) {
			throw new NullPointerException("Nonexistent directory");
		}
		
		if(file.listFiles().length==0) {
			return result;
		}
		
		for(File f:file.listFiles()) {					
			
			if(!f.getName().endsWith(".jar"))continue;
			
			try {
				cls=findClass(f);
				result.add((ObserverPlugin) cls.getDeclaredConstructor().newInstance());
			} catch (IOException e) {				
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
	}
				
		return result;
	}
	
	private Class<?> findClass(File file) throws IOException, ClassNotFoundException{
		JarFile jarFile = new JarFile(file);
		Class<?> cls=null;	
		Enumeration<JarEntry> e = jarFile.entries();
		String className="";
		
		if(!e.hasMoreElements()) {
			
		}
		
		while(e.hasMoreElements()) {			
			JarEntry je= e.nextElement();	
			if(je.isDirectory() || !je.getName().endsWith(".class")) {				
				continue;
			}
			className=je.getName().split("/")[je.getName().split("/").length-1];
			cls=Class.forName(className.replace(".class",""));
			//16
		}
				
		return cls;
	}

	

}
