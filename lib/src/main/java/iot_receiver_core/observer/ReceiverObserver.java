package iot_receiver_core.observer;

import iot_receiver_core.Acquisition;

public interface ReceiverObserver {
	void update(Acquisition measure);

}
