package iot_receiver_core.observer;

import iot_receiver_core.Acquisition;

public interface EventListener {
	void receive(Acquisition measure);

}
