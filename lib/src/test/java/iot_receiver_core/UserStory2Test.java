package iot_receiver_core;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import iot_receiver_core.discovery.Discovery;
import iot_receiver_core.discovery.ObserverPlugin;


public class UserStory2Test {
	private Discovery notifiesFinder;
	
	@BeforeEach
	public void setup() {
		this.notifiesFinder=new Discovery();
	}
	
	@Test
	public void acceptanceCriteria1() {
		NullPointerException thrown= Assertions.assertThrows(NullPointerException.class, ()->{
			this.notifiesFinder.find("Nonexistent");
		});
		
		Assertions.assertEquals("Nonexistent directory",thrown.getMessage().toString());
		
	}
	
	@Test
	public void acceptanceCriteria2() {
		IllegalArgumentException thrown= Assertions.assertThrows(IllegalArgumentException.class, ()->{
			this.notifiesFinder.find("./src/test/resources/archivo.txt");
		});
		
		Assertions.assertEquals("The argument must be a directory path",thrown.getMessage().toString());
		
	}
	
	@Test
	public void acceptanceCriteria3() {		
		try {
			//Given
			Set<ObserverPlugin> set=new HashSet<>();
					
			//When
			set=this.notifiesFinder.find("./src/test/resources/carpetaVacia");
			
			//Then
			Assertions.assertTrue(set.isEmpty());
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void acceptanceCriteria4() {		
		try {
			//Given
			Set<ObserverPlugin> set=new HashSet<>();
					
			//When
			set=this.notifiesFinder.find("./src/test/resources/noEsNotificador");
			
			//Then
			Assertions.assertTrue(set.isEmpty());
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void acceptanceCriteria5() throws ClassNotFoundException, InstantiationException, IllegalAccessException {			
			Set<ObserverPlugin> notifiers=this.notifiesFinder.find("./src/test/resources/notificadorSimple");

			Assertions.assertTrue(notifiers.size()==1);	
			Assertions.assertTrue(notifiers.toArray()[0].getClass().getSimpleName().equals("Email"));	
	} 
	
	@Test
	public void acceptanceCriteria6() throws ClassNotFoundException, InstantiationException, IllegalAccessException {			
			Set<ObserverPlugin> notifiers=this.notifiesFinder.find("./src/test/resources/notificadoresMultiples");
			String notifier1=notifiers.toArray()[1].getClass().getSimpleName();
			String notifier2=notifiers.toArray()[0].getClass().getSimpleName();
			
			Assertions.assertTrue(notifiers.size()==2);	
			Assertions.assertTrue(notifier1.equals("Email") || notifier1.equals("Telegram"));	
			Assertions.assertTrue(notifier2.equals("Telegram") || notifier2.equals("Email"));	
	} 

}
