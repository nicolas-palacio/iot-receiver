package iot_receiver_core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserStory1Test {

	private Receiver receiver;
	private ReceiverObserver receiverObserver;
	
	@BeforeEach
	public void setup() {		 
		 this.receiver=new Receiver();	
		 this.receiverObserver=new ReceiverObserver();
	}
	
	@Test
	public void critero1() {
		//Given
		Acquisition measure1=new Acquisition(001,44.0);		
		Acquisition measureExpected=new Acquisition(001,44.0);
				
		//When
		this.receiver.receive(measure1);
		
		
		//Then
		Assertions.assertEquals(receiver.getLastMeasure().getTemperature(), measureExpected.getTemperature());	
	}
	
	@Test
	public void criterio2() {
		//Given
		Acquisition measure2=null;
		
		//When	
	
		NullPointerException thrown= Assertions.assertThrows(NullPointerException.class, ()->{
			this.receiver.receive(measure2);
		});
		
		//Then
		Assertions.assertEquals("Measure can't be null",thrown.getMessage().toString());
		
	}
	
	@Test
	public void criterio3() {
		//Given
		Acquisition measure3=new Acquisition(null,44.0);
		
		//When	
		//this.receiver.receive(measure2);
		
		//Then
		NullPointerException thrown= Assertions.assertThrows(NullPointerException.class, ()->{
			this.receiver.receive(measure3);
		});
		
		Assertions.assertEquals("Device ID can't be null",thrown.getMessage().toString());
		
	}
	
	@Test
	public void criterio4() {
		//Given
		Acquisition measure4=new Acquisition(001,null);
		
		//When			
		NullPointerException thrown= Assertions.assertThrows(NullPointerException.class, ()->{
			this.receiver.receive(measure4);
		});
		
		//Then
		Assertions.assertEquals("Temperature can't be null",thrown.getMessage().toString());
		
	}
		
}
