package iot_receiver_core.iteration1;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import iot_receiver_core.discovery.Discovery;
import iot_receiver_core.discovery.ObserverPlugin;

public class UserStory4Test {
	//Quiero alternar el estado de un Observador
	
	private ObserverPlugin loggerPlugin;
	private Discovery discovery;
	
	@BeforeEach
	public void setup() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		this.discovery=new Discovery();		
		
		Set<ObserverPlugin> plugin=this.discovery.find("./src/test/resources/it1/observadorSimple");
		loggerPlugin=(ObserverPlugin) plugin.toArray()[0];
	}
	
	@Test
	//CA1:Habilitar un Observador configurable
	public void acceptance_criteria1() {
		loggerPlugin.changeState(false);
		
		Assertions.assertTrue(loggerPlugin.isEnable()==false);
	}
	
	@Test
	//CA2:Deshabilitar un Observador configurable
	public void acceptance_criteria2() {
		loggerPlugin.changeState(false);
		loggerPlugin.changeState(true);
		
		Assertions.assertTrue(loggerPlugin.isEnable()==true);
	}


}
