import iot_receiver_core.discovery.ObserverPlugin;

import javax.swing.JPanel;

import iot_receiver_core.Acquisition;

public class Logger implements ObserverPlugin{
	private boolean enable;
	
	public Logger() {
		this.enable=true;
	}

	@Override
	public void update(Acquisition acquisition) {
		System.out.println("A high temperature was detected.");
		System.out.println("Notification sent by Telegram.");
		
	}

	@Override
	public JPanel getJPanel() {
		// TODO Auto-generated method stub
		return new JPanel();
	}

	@Override
	public boolean isEnable() {
		// TODO Auto-generated method stub
		return this.enable;
	}

	@Override
	public void changeState(boolean state) {
		// TODO Auto-generated method stub
		this.enable=state;
	}

	
	

}
