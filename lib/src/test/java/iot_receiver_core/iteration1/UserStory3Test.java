package iot_receiver_core.iteration1;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import iot_receiver_core.discovery.Discovery;
import iot_receiver_core.discovery.ObserverPlugin;

public class UserStory3Test {
	//Quiero agregar un Observador configurable
	
	private Discovery discovery;
	
	@BeforeEach
	public void setup() {
		this.discovery=new Discovery();		
	}
	
	@Test
	//CA1: Ubicacion inexistente
	public void acceptance_criteria1() {
		NullPointerException thrown= Assertions.assertThrows(NullPointerException.class, ()->{
			this.discovery.find("Nonexistent");
		});
		
		Assertions.assertEquals("Nonexistent directory",thrown.getMessage().toString());
	}
	
	@Test
	//CA2: No es Observador Configurable
	public void acceptance_criteria2() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Set<ObserverPlugin> plugin=this.discovery.find("./src/test/resources/it1/noEsObservador");
		ObserverPlugin telegramPlugin=(ObserverPlugin) plugin.toArray()[0];
		
		Assertions.assertTrue(plugin.size()==1);	
		Assertions.assertTrue(plugin.toArray()[0].getClass().getSimpleName().equals("Telegram"));
		Assertions.assertTrue(telegramPlugin.getJPanel()==null);
	}
	
	@Test
	//CA3: Es Observador Configurable
	public void acceptance_criteria3() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Set<ObserverPlugin> plugin=this.discovery.find("./src/test/resources/it1/observadorSimple");
		ObserverPlugin loggerPlugin=(ObserverPlugin) plugin.toArray()[0];
		
		Assertions.assertTrue(plugin.size()==1);	
		Assertions.assertTrue(plugin.toArray()[0].getClass().getSimpleName().equals("Logger"));
		Assertions.assertTrue(loggerPlugin.getJPanel()!=null);
	}
	
	

}
